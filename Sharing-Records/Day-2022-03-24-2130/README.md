# Kubernetes 网络插件(CNI)简介 & Canal CNI深入分析

## 分享日期

2022-03-24 21:30

## 分享人

Chris Wang

## 分享主题

Kubernetes 网络插件(CNI)简介 & Canal CNI深入分析

## 分享链接

## B站
https://www.bilibili.com/video/BV1uL411A7KP/

### 百度网盘

链接: https://pan.baidu.com/s/1_0y3MDWuowVPHB-s1tRabg 提取码: 7prj 

### 天翼云盘

https://cloud.189.cn/web/share?code=BVRn6rbURBVn（访问码：x6i2）

### 阿里云盘
https://www.aliyundrive.com/s/oioUo12HuhN 提取码: 9lk6

没有阿里云盘又想使用的可以点击下面的链接注册，顺便我也涨点容量，用于存储每次的分享视频
```yaml
我在使用超好用的「阿里云盘」，注册就领 300 GB 容量，完成新手任务再领 500 GB，快来试试吧 🎉

------------
点此链接领取福利：
https://pages.aliyundrive.com/mobile-page/web/beinvited.html?code=05ee515
```

## 分享计划

### 在线文档

有计划分享知识的道友可以在下面的在线文档中填写相关分享信息

https://docs.qq.com/sheet/DZmJSSWlFYmRMbm1K?tab=BB08J2

### 分享群二维码

有计划分享知识的道友可以带着你的分享计划加群

![](https://gitee.com/cloud-native-sharing/cloud-native-sharing/raw/master/grcode.jpeg)
