# 基于KubeSphere的Kubernetes生产实践之路-起步篇

## 分享日期

2022-03-26 21:30

## 分享人

老Z

## 分享主题

基于KubeSphere的Kubernetes生产实践之路-起步篇

[Kubernetes-on-KubeSphere/基于KubeSphere的Kubernetes生产实践之路-起步篇.md · devops/cloudnative - Gitee.com](https://gitee.com/zdevops/cloudnative/blob/main/Kubernetes-on-KubeSphere/基于KubeSphere的Kubernetes生产实践之路-起步篇.md)

## 分享内容获取方式

### 在线文档

[Cloud Native Sharing/cloud-native-sharing - Gitee.com](https://gitee.com/cloud-native-sharing/cloud-native-sharing/tree/master)

### B站

https://www.bilibili.com/video/BV14F411W738/

### 百度网盘

链接: https://pan.baidu.com/s/10EYjMLcdJNYW-MyTp0vzcQ 提取码: c4ge 

### 天翼云盘

https://cloud.189.cn/t/iMRzimFFJ77r（访问码：qf7c）

### 阿里云盘

https://www.aliyundrive.com/s/pTtNjpWWxdx 提取码: 4hg7

没有阿里云盘又想使用的可以点击下面的链接注册，顺便我也涨点容量，用于存储每次的分享视频

```yaml
我在使用超好用的「阿里云盘」，注册就领 300 GB 容量，完成新手任务再领 500 GB，快来试试吧 🎉

------------
点此链接领取福利：
https://pages.aliyundrive.com/mobile-page/web/beinvited.html?code=05ee515
```

## 分享计划

### 在线文档

有计划分享知识的道友可以在下面的在线文档中填写相关分享信息

https://docs.qq.com/sheet/DZmJSSWlFYmRMbm1K?tab=BB08J2

### 分享群二维码

有计划分享知识的道友可以带着你的分享计划加群

![](https://gitee.com/cloud-native-sharing/cloud-native-sharing/raw/master/grcode.jpeg)